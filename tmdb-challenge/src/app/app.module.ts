import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//pages
import MoviesPage from "../components/pages/movies/movies.page";

//layout
import HeaderNav from "../components/layout/headerNav/header-nav.component";

//common components
import Card from "../components/common/card/card.component";
import StarRating from "../components/common/starRating/star-rating.component";
import Rating from "../components/common/rating/rating.component";
import RatingFilter from "../components/common/ratingFilter/rating-filter.component";

@NgModule({
  declarations: [
    AppComponent,

    MoviesPage,

    HeaderNav,

    Card,
    StarRating,
    Rating,
    RatingFilter
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
