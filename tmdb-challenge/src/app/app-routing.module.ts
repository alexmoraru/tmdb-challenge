import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import MoviesPage from "../components/pages/movies/movies.page";

const routes: Routes = [
  { path: "", redirectTo: "movies", pathMatch: "full" },
  { path: "movies", component: MoviesPage }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
