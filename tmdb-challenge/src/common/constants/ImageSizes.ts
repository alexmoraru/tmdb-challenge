export const ImageSizeEndpoints = {
    extraSmall: "w92",
    small: "w154",
    medium: "w185",
    large: "w342",
    extraLarge: "w500"
};