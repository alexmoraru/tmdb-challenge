import MoviesService from "./MoviesService";
import { TestBed } from '@angular/core/testing';
import Movie from '../models/Movie';
import { HttpClientModule } from '@angular/common/http';

describe("Movies Service", () => {
    let service: MoviesService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [MoviesService]
        });

        service = TestBed.get(MoviesService);
    });

    it("should create an instance", () => {
        expect(service).toBeDefined();
    });

    it("should order by popularity", () => {
        let movies = new Array<Movie>();
        
        let movie1 = new Movie();
        movie1.popularity = 5;

        let movie2 = new Movie();
        movie2.popularity = 3;

        let movie3 = new Movie();
        movie3.popularity = 7;

        movies.push(...[movie1, movie2, movie3]);
        
        let sortedMovies = service.sortByPopularity(movies);

        expect(sortedMovies).toBeDefined();
        expect(sortedMovies.length).toEqual(3);
        expect(sortedMovies[0].popularity).toEqual(7);
        expect(sortedMovies[1].popularity).toEqual(5);
        expect(sortedMovies[2].popularity).toEqual(3);
    });

    it("should filter by rating", () => {
        let movies = new Array<Movie>();
        
        let movie1 = new Movie();
        movie1.voteAverage = 5;

        let movie2 = new Movie();
        movie2.voteAverage = 3;

        let movie3 = new Movie();
        movie3.voteAverage = 7;

        movies.push(...[movie1, movie2, movie3]);

        let filteredMovies = service.filterByRating(movies, 2);
        expect(filteredMovies).toBeDefined();
        expect(filteredMovies.length).toEqual(3);

        filteredMovies = service.filterByRating(movies, 4);
        expect(filteredMovies).toBeDefined();
        expect(filteredMovies.length).toEqual(2);
        expect(filteredMovies[0].voteAverage).toBeGreaterThanOrEqual(4);
        expect(filteredMovies[1].voteAverage).toBeGreaterThanOrEqual(4);

        filteredMovies = service.filterByRating(movies, 6);
        expect(filteredMovies).toBeDefined();
        expect(filteredMovies.length).toEqual(1);
        expect(filteredMovies[0].voteAverage).toBeGreaterThanOrEqual(6);

        filteredMovies = service.filterByRating(movies, 8);
        expect(filteredMovies).toBeDefined();
        expect(filteredMovies.length).toEqual(0);
    });
});