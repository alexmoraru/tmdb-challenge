import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from "../../../environments/environment";
import Movie from "../../models/Movie";
import Genre from 'src/common/models/Genre';

@Injectable({
    providedIn: "root"
})
export default class MoviesDataAccessService {
    constructor(private http: HttpClient) {
    }

    public getPopularMovies(): Observable<Array<Movie>> {
        return this.http.get(`${environment.apiUrl}/movie/popular?api_key=${environment.apiKey}`)
            .pipe(map((data: any) => {
                return data.results.map(r => {
                    let movie = new Movie();

                    movie.title = r.title;
                    movie.genres = r.genre_ids.map(gi => {
                        let newGenre = new Genre();
                        newGenre.id = gi;
                        return newGenre;
                    });
                    movie.imageUrl = r.poster_path;
                    movie.stars = r.vote_average;
                    movie.popularity = r.popularity;
                    movie.voteAverage = r.vote_average;

                    return movie;
                })
            }));
    }

    public getGenres(): Observable<Array<Genre>> {
        return this.http.get(`${environment.apiUrl}/genre/movie/list?api_key=${environment.apiKey}`)
            .pipe(map((data: any) => {
                return data.genres.map(g => {
                    let genre = new Genre();

                    genre.id = g.id;
                    genre.name = g.name;

                    return genre;
                });
            }));
    }
}