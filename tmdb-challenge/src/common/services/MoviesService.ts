import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from "rxjs/operators";

import Movie from '../models/Movie';
import MoviesDataAccessService from './dataAccess/MoviesDataAccessService';

@Injectable({
    providedIn: "root"
})
export default class MoviesService {

    constructor(private dataAccessService: MoviesDataAccessService) {

    }

    public getPopularMovies(): Observable<Array<Movie>> {
        let genreObservable = this.dataAccessService.getGenres();
        let moviesObservable = this.dataAccessService.getPopularMovies();

        return forkJoin([genreObservable, moviesObservable]).pipe(map((data: any) => {
            let genres = data[0];
            let movies = data[1];

            return movies.map(m => {
                m.genres = m.genres.map(g => {
                    let foundGenre = genres.find(x => x.id === g.id);

                    return foundGenre;
                });

                return m;
            })
        }));
    }

    public sortByPopularity(movies: Array<Movie>): Array<Movie> {
        return movies.sort((x, y) => {
            return y.popularity - x.popularity;
        });
    }

    public filterByRating(movies: Array<Movie>, rating: number) {
        return movies.filter(m => m.voteAverage >= rating);
    }
}