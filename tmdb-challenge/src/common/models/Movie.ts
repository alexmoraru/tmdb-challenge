import Genre from './Genre';

export default class Movie {
    public title: String;

    public genres: Array<Genre>;

    public imageUrl: String;

    public stars: Number;

    public popularity: number;

    public voteAverage: number;
}