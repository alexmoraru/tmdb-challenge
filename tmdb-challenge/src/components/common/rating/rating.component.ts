import { Component, Input } from '@angular/core';

@Component({
    selector: "rating",
    templateUrl: "./rating.component.html",
    styleUrls: ["./rating.component.scss"]
})
export default class Rating {
    @Input() rating: number;

    private starItems: Array<any>;

    constructor() {
        this.starItems = [];
    }

    public ngOnInit(): void {
        if (this.rating === 0) {
            this.starItems.push({
                isEmpty: true,
                isFull: false
            });
            return;
        }

        let auxStars:number = this.rating;
        while (auxStars >= 1) {
            this.starItems.push({
                isFull: true,
                isEmpty: false
            });
            auxStars--;
        }
        if (auxStars > 0) {
            this.starItems.push({
                isFull: false,
                isEmpty: false
            });
        }
    }
}