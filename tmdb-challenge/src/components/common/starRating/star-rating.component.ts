import { Component, Input } from '@angular/core';

@Component({
    selector: "star-rating",
    templateUrl: "./star-rating.component.html",
    styleUrls: ["./star-rating.component.scss"]
})
export default class StarRating {
    @Input() isFull: Boolean;
    @Input() isEmpty: Boolean;
}