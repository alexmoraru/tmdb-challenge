import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: "rating-filter",
    templateUrl: "./rating-filter.component.html",
    styleUrls: ["./rating-filter.component.scss"]
})
export default class RatingFilter {
    @Input() maxRange: number;
    @Input() defaultValue: number;
    @Output() onChange: EventEmitter<any> = new EventEmitter();

    private options: Array<any>;

    constructor() {
        this.options = [];
    }

    public ngOnInit(): void {
        let integerValues = new Array(this.maxRange).fill(null).map((x, i) => i);

        this.options = [].concat(...integerValues.map(x => [x, x + 0.5]));
    }

    public onOptionSelected(rating: number): void {
        this.onChange.emit(rating);
    }
}