import { Component, Input } from '@angular/core';

import { environment } from "../../../environments/environment";
import { ImageSizeEndpoints } from "../../../common/constants/ImageSizes";
import Genre from 'src/common/models/Genre';

@Component({
    selector: "card",
    templateUrl: "./card.component.html",
    styleUrls: ["./card.component.scss"]
})
export default class Card {
    @Input() title: String;
    @Input() genres: Array<Genre>;
    @Input() stars: number;
    @Input() posterUrl: String;

    private imageBaseUrl: String;
    private starItems: Array<any>;

    constructor() {
        this.imageBaseUrl = `${environment.imageApiUrl}/${ImageSizeEndpoints.medium}`;
        this.starItems = [];
    }
}