import { Component } from "@angular/core";
import MoviesService from 'src/common/services/MoviesService';
import Movie from 'src/common/models/Movie';

@Component({
    selector: "movies-page",
    templateUrl: "./movies.page.html",
    styleUrls: ["./movies.page.scss"],
    providers: [MoviesService]
})
export default class MoviesPage {
    private displayMovies: Array<Movie>;
    private allMovies: Array<Movie>;

    private defaultRatingValue: number;
    
    constructor(private service: MoviesService) {
        this.displayMovies = [];
        this.allMovies = [];
        this.defaultRatingValue = 3;
    }

    public ngOnInit(): void {
        this.service.getPopularMovies().subscribe(data => {
            data = this.service.sortByPopularity(data);

            this.allMovies.push(...data);

            this.filterMovies(this.defaultRatingValue);
        });
    }

    public filterMovies(rating: number): void {
        this.displayMovies = this.service.filterByRating(this.allMovies, rating);
    }
}